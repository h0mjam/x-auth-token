<?php

use Illuminate\Database\Migrations\Migration;

class CreateAuthTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
        Schema::create(‘authTokens’, function($table){
            $table->string(‘auth_identifier’,36);
            $table->string('device_id', 100);
            $table->timestamps();
            $table->softDeletes();
            $table->string('public_key', 96);
            $table->string('private_key', 96);
            $table->text('token');
            $table->enum('type', array('ios','android','web'))->default('web');

            $table->primary(array('auth_identifier', 'device_id'));
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::drop(‘authTokens');
	}

}