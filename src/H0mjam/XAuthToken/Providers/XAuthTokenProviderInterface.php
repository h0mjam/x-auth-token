<?php namespace H0mjam\XAuthToken\Providers;

use Illuminate\Contracts\Auth\UserProvider;
use H0mjam\XAuthToken\XAuthToken;

/**
 * Interface XAuthTokenProviderInterface
 * @package H0mjam\XAuthToken\Providers
 */
interface XAuthTokenProviderInterface {

    /**
     * Creates an auth token for user.
     *
     * @param UserProvider $user
     * @param string $type
     * @param $deviceId
     * @return mixed
     */
    public function create(UserProvider $user, $type, $deviceId);

    /**
     * Find user id from auth token.
     *
     * @param $serializedAuthToken
     * @return mixed
     */
    public function find($serializedAuthToken);

    /**
     * Returns serialized token.
     *
     * @param XAuthToken $token
     * @return mixed
     */
    public function serializeToken(XAuthToken $token);

    /**
     * Deserializes token.
     *
     * @param $payload
     * @return mixed
     */
    public function deserializeToken($payload);

    /**
     * Purge token
     *
     * @param $identifier
     * @param $type
     * @param $deviceId
     * @return mixed
     */
    public function purge($identifier, $type, $deviceId);
}