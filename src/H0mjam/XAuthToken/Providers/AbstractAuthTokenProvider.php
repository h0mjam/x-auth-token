<?php namespace H0mjam\XAuthToken\Providers;

use Illuminate\Encryption\DecryptException;
use Illuminate\Encryption\Encrypter;
use H0mjam\XAuthToken\XAuthToken;

/**
 * Class AbstractAuthTokenProvider
 * @package H0mjam\XAuthToken\Providers
 */
abstract class AbstractAuthTokenProvider implements XAuthTokenProviderInterface {

    /**
     * @var \Illuminate\Encryption\Encrypter
     */
    protected $encrypter;

    /**
     * @var HashProvider
     */
    protected $hasher;

    /**
     * @return HashProvider
     */
    public function getHasher(){
        return $this->hasher;
    }

    /**
     * @param Encrypter $encrypter
     * @param HashProvider $hasher
     */
    function __construct(Encrypter $encrypter, HashProvider $hasher){
        $this->encrypter    = $encrypter;
        $this->hasher       = $hasher;
    }

    /**
     * Generate new token
     *
     * @param null $publicKey
     * @return XAuthToken
     */
    protected  function generateAuthToken($publicKey = null){
        if(empty($publicKey))
        $publicKey  = $this->hasher->make();
        $privateKey = $this->hasher->makePrivate($publicKey);

        return new XAuthToken(null, $publicKey, $privateKey);
    }

    /**
     * Verify token
     *
     * @param XAuthToken $token
     * @return bool
     */
    protected function verifyAuthToken(XAuthToken $token) {
        return $this->hasher->check($token->getPublicKey(), $token->getPrivateKey());
    }


    /**
     * @param XAuthToken $token
     * @return string
     */
    public function serializeToken(XAuthToken $token){
        $payload = array(
            'id'    => $token->getAuthIdentifier(),
            'key'   => $token->getPublicKey()
        );
        return $this->encrypter->encrypt($payload);
    }

    /**
     * Deserializes token.
     *
     * @param $payload
     * @return XAuthToken|mixed|null
     */
    public function deserializeToken($payload){

        try {
            $data = $this->encrypter->decrypt($payload);
        } catch (DecryptException $e) {
            return null;
        }

        if(empty($data['id']) || empty($data['key']))
            return null;

        $token = $this->generateAuthToken($data['key']);
        $token->setAuthIdentifier($data['id']);

        return $token;
    }
}