<?php namespace H0mjam\XAuthToken\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class XAuthToken
 * @package H0mjam\XAuthToken\Facades
 */
class XAuthToken extends Facade {
    protected static function getFacadeAccessor() { return 'mannine.xauth.token'; }
}