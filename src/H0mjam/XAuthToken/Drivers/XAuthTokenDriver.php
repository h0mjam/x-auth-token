<?php

namespace H0mjam\XAuthToken\Drivers;

use Illuminate\Contracts\Auth\UserProvider;
use H0mjam\XAuthToken\XAuthToken;
use H0mjam\XAuthToken\Providers\XAuthTokenProviderInterface;

class XAuthTokenDriver {

    /**
     * @var \H0mjam\XAuthToken\Providers\XAuthTokenProviderInterface
     */
    protected $tokens;

    /**
    * @var \Illuminate\Contracts\Auth\UserProvider;
    */
    protected $users;

    /**
     * @param XAuthTokenProviderInterface $tokens
     * @param UserProvider $users
     */
    function __construct(XAuthTokenProviderInterface $tokens, UserProvider $users){
        $this->tokens = $tokens;
        $this->users = $users;
    }

    /**
     * Returns the AuthTokenInterface provider.
     */
    public function getProvider(){
        return $this->tokens;
    }

    /**
     * Validates a public auth token. Returns User object on success, otherwise false.
     *
     * @param $authTokenPayload
     * @return bool|UserProvider|null
     */
    public function validate($authTokenPayload) {

        if($authTokenPayload == null)   return false;

        $tokenResponse = $this->tokens->find($authTokenPayload);

        if($tokenResponse == null)      return false;

        $user = $this->users->retrieveByID($tokenResponse->getAuthIdentifier());

        if($user == null)               return false;

        return $user;
    }

    /**
     * Attempt to create an AuthToken from user credentials.
     *
     * @param array $credentials
     * @return bool
     */
    public function attempt(array $credentials) {
        $user = $this
            ->users
            ->retrieveByCredentials($credentials);

        if($user instanceof UserInterface && $this->users->validateCredentials($user, $credentials)) {
            return $this->create($user, false, false);
        }

        return false;
    }

    /**
     * Create auth token for user.
     *
     * @param UserProvider $user
     * @param $type
     * @param $deviceId
     * @return mixed
     */
    public function create(UserProvider $user, $type, $deviceId) {
        $this->tokens->purge($user, $type, $deviceId);
        return $this->tokens->create($user, $type, $deviceId);
    }


    /**
    * Retrive user from auth token.
    *
    * @param XAuthToken $token
    * @return UserInterface|null
    */
    public function user(XAuthToken $token) {
        return $this->users->retrieveByID( $token->getAuthIdentifier() );
    }

    /**
     * Serialize token for public use.
     *
     * @param XAuthToken $token
     * @return string
     */
    public function publicToken(XAuthToken $token) {
        return $this->tokens->serializeToken($token);
    }
}