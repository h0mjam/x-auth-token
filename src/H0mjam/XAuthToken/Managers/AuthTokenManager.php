<?php namespace H0mjam\XAuthToken\Managers;

use Illuminate\Support\Manager;
use H0mjam\XAuthToken\Drivers\XAuthTokenDriver;
use H0mjam\XAuthToken\Providers\HashProvider;
use H0mjam\XAuthToken\Providers\DatabaseAuthTokenProvider;


/**
 * Class XAuthTokenManager
 * @package H0mjam\XAuthToken\Managers
 */
class XAuthTokenManager extends Manager {

    protected function createDatabaseDriver() {
        $provider   = $this->createDatabaseProvider();
        $users      = $this->app['auth']->getProvider();

        return new XAuthTokenDriver($provider, $users);
    }

    /**
     * @return DatabaseAuthTokenProvider
     */
    protected function createDatabaseProvider() {
        $connection = $this->app['db']->connection();
        $encrypter  = app('encrypter');
        $hasher     = new HashProvider($this->app['config']['app']['key']);

        return new DatabaseAuthTokenProvider($connection, 'authTokens', $encrypter, $hasher);
    }

    /**
     * @return string
     */
    public function getDefaultDriver() {
        return 'database';
    }
}