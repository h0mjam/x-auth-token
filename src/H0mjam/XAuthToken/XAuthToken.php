<?php namespace H0mjam\XAuthToken;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Class XAuthToken
 * @package H0mjam\XAuthToken
 */
class XAuthToken implements Arrayable {

    /**
     * @var
     */
    protected $authIdentifier;

    /**
     * @var
     */
    protected $publicKey;

    /**
     * @var
     */
    protected $privateKey;

    /**
     * @var null
     */
    protected $deviceId = null;

    /**
     * @var null
     */
    protected $token    = null;

    /**
     * @var string
     */
    protected $type     = "web";

    /**
     * @param $authIdentifier
     * @param $publicKey
     * @param $privateKey
     */
    function __construct($authIdentifier, $publicKey, $privateKey){
        $this->authIdentifier   = $authIdentifier;
        $this->publicKey        = $publicKey;
        $this->privateKey       = $privateKey;
    }

    /**
     * @return mixed
     */
    public function getAuthIdentifier(){
        return $this->authIdentifier;
    }

    /**
     * @param $authIdentifier
     */
    public function setAuthIdentifier($authIdentifier){
        $this->authIdentifier = $authIdentifier;
    }

    /**
     * @return mixed
     */
    public function getPrivateKey(){
        return $this->privateKey;
    }

    /**
     * @return mixed
     */
    public function getPublicKey(){
        return $this->publicKey;
    }

    /**
     * @return null
     */
    public function getDeviceId(){
        return $this->deviceId;
    }

    /**
     * @return null
     */
    public function getToken(){
        return $this->token;
    }

    /**
     * @return string
     */
    public function getType(){
        return $this->type;
    }

    /**
     * @param $privateKey
     */
    public function setPrivateKey($privateKey){
        $this->privateKey = $privateKey;
    }

    /**
     * @param $publicKey
     */
    public function setPublicKey($publicKey){
        $this->publicKey = $publicKey;
    }

    /**
     * @param $deviceId
     */
    public function setDeviceID($deviceId){
        $this->deviceId = $deviceId;
    }

    /**
     * @param $token
     */
    public function setToken($token){
        $this->token = $token;
    }

    /**
     * @param string $type
     */
    public function setType($type){
        $this->type = $type;
    }

    /**
    * Get the instance as an array.
    *
    * @return array
    */
    public function toArray(){
        return array(
            'auth_identifier'   => $this->authIdentifier,
            'public_key'        => $this->publicKey,
            'private_key'       => $this->privateKey,
            'device_id'         => $this->deviceId,
            'token'             => $this->token,
            'type'              => $this->type
        );
    }
}