<?php namespace H0mjam\XAuthToken;

use Illuminate\Support\ServiceProvider;
use H0mjam\XAuthToken\Filters\XAuthTokenFilter;
use H0mjam\XAuthToken\Managers\XAuthTokenManager;

/**
 * Class XAuthTokenServiceProvider
 * @package H0mjam\XAuthToken
 */
class XAuthTokenServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;


    public function boot(){}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register(){

        $app = $this->app;

        $this->app['mannine.xauth.token'] = $this->app->share(function($app) {
            return new XAuthTokenManager($app);
        });

        $this->app['mannine.xauth.token.filter'] = $this->app->share(function($app) {
            $driver = $app['mannine.xauth.token']->driver();
            $events = $app['events'];

            return new XAuthTokenFilter($driver, $events);
        });

        $app->singleton('H0mjam\XAuthToken\XAuthTokenController', function ($app) {
            $driver         = $app['mannine.xauth.token']->driver();
			//print_r(config());
            $credsFormatter = $app['config']->get('laravel-auth-token::format_credentials', null);
            return new XAuthTokenController($driver, $credsFormatter);
        });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides(){
        return array('mannine.xauth.token', 'mannine.xauth.token.filter');
	}

}