<?php namespace H0mjam\XAuthToken\Exceptions;

/**
 * Class NotAuthorizedException
 * @package H0mjam\XAuthToken\Exceptions
 */
class NotAuthorizedException extends \Exception {


    /**
     * Simple exception
     *
     * @param string $message
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($message = "Not Authorized", $code = 401, Exception $previous = null){
        parent::__construct($message, $code, $previous);
    }

}