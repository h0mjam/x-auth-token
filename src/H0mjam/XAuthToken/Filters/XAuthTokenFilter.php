<?php namespace H0mjam\XAuthToken\Filters;

use App\Events\XAuthTokenValid;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Events\Dispatcher;
use H0mjam\XAuthToken\Exceptions\NotAuthorizedException;
use H0mjam\XAuthToken\Drivers\XAuthTokenDriver;

/**
 * Class XAuthTokenFilter
 * @package H0mjam\XAuthToken\Filters
 */
class XAuthTokenFilter {

    /**
    * The event dispatcher instance.
    *
    * @var \Illuminate\Events\Dispatcher
    */
    protected $events;

    /**
     * @var \H0mjam\XAuthToken\Drivers\XAuthTokenDriver
     */
    protected $driver;

    /**
     * __construct
     *
     * @param XAuthTokenDriver $driver
     * @param Dispatcher $events
     */
    function __construct(XAuthTokenDriver $driver, Dispatcher $events){
        $this->driver = $driver;
        $this->events = $events;
    }

    /**
     * Check auth filter
     * @param $request
     * @param $next
     * @return
     * @throws NotAuthorizedException
     * @internal param $route
     */
    function handle($request, $next) {
        $payload = $request->header('X-Auth-Token');

        if(empty($payload))
            $payload = $request->input('auth_token');

        try {
            $user = $this->driver->validate($payload);
        } catch (DecryptException $e) {
            throw new NotAuthorizedException();
        }

        if(!$user)
            throw new NotAuthorizedException();

        event(new XAuthTokenValid($user));

        return $next($request);
    }
}