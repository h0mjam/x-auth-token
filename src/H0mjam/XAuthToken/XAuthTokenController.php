<?php namespace H0mjam\XAuthToken;

use App\Exceptions\WrongDeviceIDException;
use App\Exceptions\WrongDeviceTypeException;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Rest;
use Illuminate\Http\Request;
use H0mjam\XAuthToken\Exceptions\NotAuthorizedException;
use H0mjam\XAuthToken\Drivers\XAuthTokenDriver;

/**
 * Class XAuthTokenController
 * @package H0mjam\XAuthToken
 */
class XAuthTokenController extends Controller {
    protected $driver;

    /**
     * @param XAuthTokenDriver $driver
     */
    function __construct(XAuthTokenDriver $driver){
        $this->driver = $driver;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function getAuthToken(Request $request) {
        $token = $request->header('X-Auth-Token');

        if(empty($token))
            $token = $request->only('auth_token');

        return $token;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws NotAuthorizedException
     */
    public function index(Request $request) {
        $payload = $this->getAuthToken($request);
        $user = $this->driver->validate($payload);

        if(!$user) {
            return Rest::response(403, trans('response.403'));
        } else {
            return Rest::response(200, $user);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws NotAuthorizedException
     */
    public function store(Request $request) {

        $input = $request->all();

        $validator = \Validator::make(
            $input, [
                'email'  => ['required'],
                'password'  => ['required'],
                'deviceID' => ['required'],
            ]
        );

        if($validator->fails())
            throw new NotAuthorizedException();

        $token = $this->driver->attempt($input);

        if(!$token)
            throw new NotAuthorizedException();

        $serializedToken    = $this->driver->getProvider()->serializeToken($token);
        $user               = $this->driver->user($token);

        return Rest::response(200, ['token' => $serializedToken, 'user' => $user->toArray()]);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws NotAuthorizedException
     * @throws WrongDeviceIDException
     * @throws WrongDeviceTypeException
     */
    public function destroy(Request $request) {
        $payload = $this->getAuthToken($request);
        $user = $this->driver->validate($payload);
        $input = $request->only('deviceID', 'deviceType');

        if (!$user) {
            return Rest::response(403, ['code' => 403], trans('response.403'));
        }

        if ($input['deviceID'] == null) {
            return Rest::response(409, ['code' => 108], trans('response.108'));
        }

        if (!in_array($input['deviceType'], ['web', 'ios', 'android'])) {
            return Rest::response(409, ['code' => 107], trans('response.107'));
        }

        $this->driver->getProvider()->purge($user, $input['deviceType'], $input['deviceID']);

        return Rest::response(200, array('success' => true));
    }
}