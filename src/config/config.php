<?php

/**
 * Credentials
 * @return array
 */
return array(

	'format_credentials' => function ($username, $password) {
		return array(
			'email'     => $username,
			'password'  => $password
		);
	}

);